/*
 * This file was automatically generated by EvoSuite
 */

package org.character.net.packet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import org.character.net.packet.ClientConnect;
import org.javathena.core.net.SessionPacket;
import org.junit.BeforeClass;

@RunWith(EvoSuiteRunner.class)
public class ClientConnectEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      ClientConnect clientConnect0 = new ClientConnect();
      byte[] byteArray0 = new byte[2];
      SessionPacket sessionPacket0 = new SessionPacket(byteArray0);
      // Undeclared exception!
      try {
        clientConnect0.execute(sessionPacket0);
        fail("Expecting exception: NullPointerException");
      } catch(NullPointerException e) {
      }
  }
}
